const webpack = require('webpack');
const path = require('path');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const HTMLPlugin = require('html-webpack-plugin');
const { SplitChunksPlugin } = webpack.optimize;
const { NODE_ENV } = process.env;
const isDev = NODE_ENV !== 'production';

const SRC = path.resolve(__dirname, './front');
const OUTPUT = path.resolve(__dirname, './output');

module.exports = {
  entry: [`${SRC}/index.jsx`],
  target: 'web',
  devtool: isDev ? 'source-map' : false,
  mode: isDev ? 'development' : 'production',
  output: {
    path: OUTPUT,
    filename: '[name]-[hash].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.less/,
        include: SRC,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              camelCase: true,
              localIdentName: '[name]-[local]-[hash:base64:5]',
            },
          },
          'less-loader',
        ],
      },
      {
        test: /.*\.(gif|png|jpe?g|svg|swf)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[hash].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HTMLPlugin({
      template: `${SRC}/index.ejs`,
    }),
    new SplitChunksPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      components: `${SRC}/components`,
      pages: `${SRC}/pages`,
      hocs: `${SRC}/hocs`,
      utils: `${SRC}/utils`,
    },
  },
  devServer: {
    port: 3004,
    contentBase: OUTPUT,
    hot: true,
  },
};

if (process.env.ANALYZE) {
  module.exports.plugins.push(new BundleAnalyzerPlugin());
}
