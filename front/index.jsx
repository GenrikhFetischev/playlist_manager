import React from 'react';
import { render } from 'react-dom';
import App from './app.jsx';
import './global.less'

const root = document.getElementById('app');
render(<App />, root);