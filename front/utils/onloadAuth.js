import fetch from 'utils/fetch';
import { login } from 'components/auth/login/redux';
import cookie from 'browser-cookies';

const onloadAuth = async dispatch => {
  const token = cookie.get('token');
  if (!token) return;
  const { items: [user] } = await fetch('/auth/user/me', { method: 'POST' }).then(resp =>
    resp.json(),
  );
  dispatch(login({ user, token }));
};

export default onloadAuth;
