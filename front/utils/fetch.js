import fetch from 'isomorphic-fetch';
import cookie from 'browser-cookies';

const customFetch = (url, options) => {
  const token = cookie.get('token');
  if (!options) return fetch(url, { headers: { authorization: token } });
  const { headers = {} } = options;
  headers['content-type'] = headers['content-type'] || 'application/json';
  headers.authorization = token;
  return fetch(url, {
    ...options,
    headers: {
      ...options.headers,
      ...headers,
    },
  });
};

export default customFetch;
