import React from 'react';
import { Provider } from 'react-redux';
import onloadAuth from 'utils/onloadAuth';

import store from './redux';
import AppRouter from './router/index';

onloadAuth(store.dispatch);

const App = () => (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

export default App;
