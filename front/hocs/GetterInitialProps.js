import React, { Component } from 'react';

const GetterInitialProps = Page => {
  class WrappedComponent extends Component {
    state = {};

    static getDerivedStateFromProps() {
      if (typeof Page.getInitialProps !== 'function') return { render: true };
      return null
    }

    async componentDidMount() {
      if (!Page.getInitialProps) return
      const initialProps = await Page.getInitialProps(this.props);
      this.setState({ initialProps, render: true });
    }

    render() {
      const { render, initialProps } = this.state;
      if (!render) return <div>preloader</div>;
      return <Page {...this.props} initialProps={initialProps} />;
    }
  }
  return WrappedComponent;
};

export default GetterInitialProps;
