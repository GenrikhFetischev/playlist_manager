import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from './reducer';

const NODE_ENV = process.env.NODE_ENV || 'production';
const store =
  NODE_ENV === 'production'
    ? createStore(reducer, applyMiddleware(thunk))
    : createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
export default store;
