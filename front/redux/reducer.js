import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import auth from 'components/auth/login/redux';

export default combineReducers({ auth, form });
