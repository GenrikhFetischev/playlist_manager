import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'utils/fetch';
import { Modal, Button, TextField, CardContent, Card, CardHeader } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import s from './RemoveConfirm.less';

class RemoveConfirm extends Component {
  state = {
    value: '',
    error: false,
    deleted: false,
  };

  onChange = event => {
    const { value } = event.target;
    this.setState({ value, error: false });
  };

  async removeItem(url, id) {
    const body = JSON.stringify({ id });
    const response = await fetch(url, { body, method: 'POST' }).then(response => response.json());
    this.setState({ response });
  }

  submit = () => {
    const { value } = this.state;
    const { confirmString, id, url } = this.props;
    if (value === confirmString) this.removeItem(url, id);
    else this.setState({ error: true });
  };

  render() {
    const { confirmString, close, redirect } = this.props;
    const { onChange, submit, state: { value, error, response = {} } } = this;
    if (response.status === 'DELETED') return <Redirect to={redirect} />;
    return (
      <Modal className={s.wrapper} open={true} onClose={close}>
        <Card className={s.card}>
          <CardHeader title={`Чтобы удалить элемент, введите без ковычек: "${confirmString}"`} />
          <CardContent className={s.content}>
            <TextField
              label="Введите подтверждение"
              onChange={onChange}
              autoFocus={true}
              placeholder={confirmString}
              error={error}
              value={value}
            />
            <Button variant="contained" color="secondary" onClick={submit}>
              Удалить
            </Button>
          </CardContent>
        </Card>
      </Modal>
    );
  }
}

RemoveConfirm.propTypes = {
  url: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  confirmString: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  redirect: PropTypes.string.isRequired,
};

export default RemoveConfirm;
