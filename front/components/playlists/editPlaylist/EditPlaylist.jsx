import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import RemoveConfirm from 'components/modal/removeConfirm';
import { Field, reduxForm, Form } from 'redux-form';
import {
  Checkbox,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  IconButton,
  Card,
  CardHeader,
  CardContent,
  Button,
  Typography,
  Paper,
} from '@material-ui/core';
import { KeyboardArrowUp as ArrowUp, KeyboardArrowDown as ArrowDown } from '@material-ui/icons';
import TextInput from 'components/form/textInput';
import s from './EditPlaylist.less';

class EditPlaylist extends Component {
  constructor(props) {
    super(props);
    const { initialValues = {}, songList = [] } = props;
    props.initialize({
      ...initialValues,
      date: EditPlaylist.parseDate(initialValues.date),
    });
    this.state = {
      songList: songList
        .map(song => {
          if (initialValues.Songs && initialValues.Songs.find(s => s.id === song.id)) {
            song.checked = true;
          }
          return song;
        })
        .filter(song => song.isCompleted),
      selected: initialValues.Songs || [],
      showQuestion: false,
    };
  }

  static parseDate = d => {
    const date = d ? new Date(d) : new Date(Date.now());
    const year = date.getFullYear();
    const month = date.getMonth().length > 1 ? date.getMonth() : `0${date.getMonth()}`;
    const day = date.getDay().length > 1 ? date.getDay() : `0${date.getDay()}`;
    return `${year}-${month}-${day}`;
  };

  toggleSong = id => {
    const { songList, selected } = this.state;
    let changedSelected = [...selected];
    const changedList = songList.map(song => {
      if (song.id !== id) return song;
      song.checked = !song.checked;
      if (song.checked) changedSelected.push(song);
      else changedSelected = changedSelected.filter(song => song.id !== id);
      return song;
    });
    this.setState({ songList: changedList, selected: changedSelected });
  };

  changeSelectedSongPosition = (id, isUp) => {
    const { selected } = this.state;
    const index = selected.reduce((acc, cur, key) => (cur.id === id ? key : acc), null);
    if (!index && isUp) return;
    if (index === selected.length - 1 && !isUp) return;
    const newSelected = [...selected];
    newSelected[index] = isUp ? selected[index - 1] : selected[index + 1];
    if (isUp) {
      newSelected[index - 1] = selected[index];
    } else {
      newSelected[index + 1] = selected[index];
    }
    this.setState({ selected: newSelected });
  };

  onSubmit = data => {
    const { onSubmit } = this.props;
    const { selected } = this.state;
    onSubmit({
      ...data,
      date: new Date(data.date).toUTCString(),
      songList: selected.map(({ id }) => id),
    });
  };

  deleteSong = () => {
    this.setState({ showQuestion: true });
  };

  hidePopup = () => {
    this.setState({ showQuestion: false });
  };

  render() {
    const { toggleSong, changeSelectedSongPosition, onSubmit, deleteSong, hidePopup } = this;
    const { songList, selected, showQuestion } = this.state;
    const { handleSubmit, response, fetching, initialValues = {} } = this.props;
    const { id, place } = initialValues;
    return (
      <Fragment>
        {showQuestion && (
          <RemoveConfirm
            url="/api/playlists/delete"
            confirmString={place}
            id={id}
            close={hidePopup}
            redirect="/playlists"
          />
        )}
        <Paper className={s.paper}>
          <div className={s.wrapper}>
            <Form onSubmit={handleSubmit(onSubmit)} className={s.form}>
              <Field component={TextInput} name="place" label="Место" className={s.input} />
              <Field
                component={TextInput}
                name="date"
                label="Дата"
                type="date"
                className={s.input}
              />
              <div>
                <List className={s.list}>
                  {songList.map(({ id, name, checked }) => (
                    <ListItem key={id} dense button>
                      <ListItemText className={s.listItem}>{name}</ListItemText>
                      <ListItemSecondaryAction>
                        <Checkbox onChange={() => toggleSong(id)} checked={checked} />
                      </ListItemSecondaryAction>
                    </ListItem>
                  ))}
                </List>
              </div>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                className={s.button}
                disabled={fetching || !selected.length}
              >
                Сохранить
              </Button>
              {response && (
                <Typography className={s.response} color="secondary">
                  {response.status === 'OK' ? 'Сохранено' : 'Возникла ошибка, повторите позднее'}
                </Typography>
              )}
              {id && (
                <div className={s.remove}>
                  <Button variant="outlined" onClick={deleteSong} size="small" className={s.button}>
                    Удалить плейлист
                  </Button>
                </div>
              )}
            </Form>
            <Card className={s.selected}>
              <CardHeader title="Песни в плейлисте" />
              <CardContent>
                <List className={s.list}>
                  {selected.map(({ id, name }, index) => (
                    <ListItem key={id} dense button>
                      <ListItemText className={s.listItem}>{`${index + 1}. ${name}`}</ListItemText>
                      <ListItemSecondaryAction>
                        <IconButton
                          color="secondary"
                          onClick={() => changeSelectedSongPosition(id, true)}
                        >
                          <ArrowUp />
                        </IconButton>
                        <IconButton color="primary" onClick={() => changeSelectedSongPosition(id)}>
                          <ArrowDown />
                        </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  ))}
                </List>
              </CardContent>
            </Card>
          </div>
        </Paper>
      </Fragment>
    );
  }
}

EditPlaylist.propTypes = {
  songList: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func,
  initialize: PropTypes.func,
  editable: PropTypes.bool,
  response: PropTypes.object,
  fetching: PropTypes.bool,
  initialValues: PropTypes.shape({
    place: PropTypes.string,
    date: PropTypes.string,
    songs: PropTypes.array,
    id: PropTypes.string,
  }),
};

export default reduxForm({
  form: 'Editable playlist',
})(EditPlaylist);
