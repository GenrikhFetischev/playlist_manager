import React, { Component } from 'react';
import propTypes from 'prop-types';
import cn from 'classnames';
import { Typography, Card, CardContent, CardHeader, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import SongDetails from 'components/songs/songDetails';
import ScrollWrapper from 'components/scrollWrapper';

import s from './PlaylistDetails.less';

class PlaylistDetails extends Component {
  state = {};

  parseDate = date => new Date(date).toLocaleDateString();

  onSongClick = id => {
    const { onSongClick } = this.props;
    if (onSongClick) onSongClick(id);
    this.setState({ selectedSong: id });
  };

  render() {
    const { selectedSong } = this.state;
    const { parseDate, onSongClick } = this;
    const { place, date, Songs = [], id } = this.props.playlist;
    return (
      <div className={s.wrapper}>
        <Card className={s.playlist}>
          <CardHeader
            title={place}
            subheader={parseDate(date)}
            action={
              <Button size="small">
                <Link to={`/playlists/${id}`} className={s.edit}>
                  Редактировать
                </Link>
              </Button>
            }
          />
          <CardContent>
            {Songs.map(({ name, id }, key) => (
              <Typography
                key={id}
                onClick={() => onSongClick(id)}
                className={cn(s.song, id === selectedSong ? s.selected : '')}
              >{`${key + 1} ${name} `}</Typography>
            ))}
          </CardContent>
        </Card>
        {selectedSong && (
          <ScrollWrapper className={s.songDetails}>
            <SongDetails
              key={selectedSong}
              song={Songs.find(song => song.id === selectedSong)}
              className={s.songCard}
            />
          </ScrollWrapper>
        )}
      </div>
    );
  }
}

PlaylistDetails.propTypes = {
  playlist: propTypes.shape({
    place: propTypes.string,
    date: propTypes.string,
    Songs: propTypes.arrayOf(propTypes.object),
    id: propTypes.string,
  }),
  onSongClick: propTypes.func,
};

export default PlaylistDetails;
