import React, { Component } from 'react';
import propTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import QueueMusic from '@material-ui/icons/QueueMusic';
import Typography from '@material-ui/core/Typography';
import s from './PlaylistSnippet.less';

class PlaylistSnippet extends Component {
  onClick = () => {
    const { item: { id }, onClick } = this.props;
    if (onClick) onClick(id);
  };

  parseDate = date => new Date(date).toLocaleDateString();

  render() {
    const { onClick, parseDate } = this;
    const { place, date, Songs } = this.props.item;
    return (
      <Card onClick={onClick} className={s.wrapper}>
        <CardContent className={s.content}>
          <Avatar>
            <QueueMusic />
          </Avatar>
          <div>
            <Typography variant="headline" component="h3" className={s.name}>
              {place}
            </Typography>
            <Typography variant="subheading" className={s.date}>
              {parseDate(date)}
            </Typography>
          </div>
          {Songs[0] &&
            Songs.length && (
              <Typography variant="headline" className={s.songs}>
                песен: {Songs.length}
              </Typography>
            )}
        </CardContent>
      </Card>
    );
  }
}

PlaylistSnippet.propTypes = {
  item: propTypes.shape({
    place: propTypes.string.isRequired,
    date: propTypes.string.isRequired,
    Songs: propTypes.arrayOf(propTypes.object),
    id: propTypes.string.isRequired,
  }),
  onClick: propTypes.func,
};

export default PlaylistSnippet;
