import React from 'react';
import PropTypes from 'prop-types';
import Header from 'components/header/index';
import s from './MainLayout.less';


const MainLayout = ({ children }) => {
  return (
    <div className={s.appWrapper}>
      <Header />
      <main className={s.content}>
        {children}
      </main>
    </div>
  );
};

MainLayout.propTypes = {
  children: PropTypes.element,
};

export default MainLayout;
