import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Typography, Card, CardContent } from '@material-ui/core';
import s from './Breadcrumbs.less';

const Breadcrumbs = ({ crumbs }) => (
  <div>
    <Card className={s.wrapper}>
      <CardContent>
        <Typography>
          {crumbs.map(({ label, to }) => (
            <Link className={s.link} to={to} key={to}>
              {label}
            </Link>
          ))}
        </Typography>
      </CardContent>
    </Card>
  </div>
);

Breadcrumbs.propTypes = {
  crumbs: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      to: PropTypes.string,
    }),
  ),
};

export default Breadcrumbs;
