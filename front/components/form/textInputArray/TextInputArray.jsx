import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextInput from 'components/form/textInput';
import { Field } from 'redux-form';
import { Button } from '@material-ui/core';
import {Add} from '@material-ui/icons';
import cn from 'classnames'
import s from './TextInputArray.less'

class TextInputArray extends Component {
  render() {
    const { fields, meta: { error, submitFailed }, arrayName, fieldCaption, className  } = this.props;
    return (
      <div className={cn(s.wrapper, className)}>
       <div className={s.main}>
         {fields.map((field, index) => (
           <Field
             key={index}
             name={`${arrayName}[${index}]`}
             component={TextInput}
             label={`${fieldCaption} ${index + 1}`}
           />
         ))}
       </div>
        <div className={s.footer}>
          <Button variant="fab" mini  aria-label="add" onClick={() => fields.push('')}>
            <Add/>
          </Button>
          {submitFailed && error && <span>{error}</span>}
        </div>
      </div>
    );
  }
}

TextInputArray.propTypes = {
  fields: PropTypes.object,
  meta: PropTypes.object,
  arrayName: PropTypes.string.isRequired,
  fieldCaption: PropTypes.string,
  className: PropTypes.string
};

export default  TextInputArray;
