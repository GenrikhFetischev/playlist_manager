import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

const TextInput = ({ label = '', input: { value, onChange }, meta: { error }, ...props }) => {
  return (
    <TextField {...props} label={error || label} value={value} onChange={onChange} error={!!error} />
  );
};

TextInput.propTypes = {
  label: PropTypes.string.isRequired,
  input: PropTypes.shape({
    value: PropTypes.string,
    onChange: PropTypes.func,
  }),
  meta: PropTypes.object,
};

export default TextInput;
