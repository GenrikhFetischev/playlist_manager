import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames'
import { Switch, FormControlLabel } from '@material-ui/core';

const SwitchInput = ({ input: { value, onChange }, label, className }) => (
  <FormControlLabel
    control={<Switch onChange={onChange} checked={!!value} />}
    label={label}
    className={cn(className)}
  />
);

SwitchInput.propTypes = {
  input: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    onChange: PropTypes.func,
  }),
  label: PropTypes.string,
  className: PropTypes.string,
};

export default SwitchInput;
