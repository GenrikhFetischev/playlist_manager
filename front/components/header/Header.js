import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { AppBar, Typography, Menu, MenuItem, IconButton } from '@material-ui/core';
import { Menu as MenuIcon, Home } from '@material-ui/icons';
import MusicNote from '@material-ui/icons/MusicNote';
import { Link, withRouter } from 'react-router-dom';
import s from './Header.less';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      loginPanel: false,
    };
  }

  handleClose = () => this.setState({ anchorEl: null });
  openMenu = e => this.setState({ anchorEl: e.currentTarget });

  render() {
    const { pathname } = this.props.location;
    const { handleClose, openMenu } = this;
    const { anchorEl } = this.state;
    const { auth } = this.props;
    return (
      <div className={s.wrapper}>
        <AppBar position="static" className={s.appBar}>
          {auth ? (
            <IconButton color="inherit" onClick={openMenu}>
              <MenuIcon />
            </IconButton>
          ) : (
            <IconButton color="inherit">
              <MusicNote />
            </IconButton>
          )}
          {pathname !== '/' && (
            <Link to="/" style={{ color: 'white' }}>
              <IconButton color="inherit">
                <Home />
              </IconButton>
            </Link>
          )}
          <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
            <MenuItem onClick={handleClose}>
              <Link className={s.link} to="/songs">
                Песни
              </Link>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <Link className={s.link} to="/playlists">
                Плейлисты
              </Link>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <Link className={s.link} to="/song/create">
                Добавить песню
              </Link>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <Link className={s.link} to="/playlist/create">
                Добавить плейлист
              </Link>
            </MenuItem>
          </Menu>

          <div className={s.text}>
            <Typography color="inherit">SONGS MANAGER</Typography>
          </div>
          {auth && (
            <Typography color="inherit">
              <Link className={s.user} to="lk">
                {auth.user.name || auth.user.login}
              </Link>
            </Typography>
          )}
        </AppBar>
      </div>
    );
  }
}

Header.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  auth: PropTypes.oneOfType([PropTypes.object]),
};

const mapStateToProps = ({ auth }) => ({
  auth,
});

export default withRouter(connect(mapStateToProps)(Header));
