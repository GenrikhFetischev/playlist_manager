import React from 'react';
import propTypes from 'prop-types';
import cn from 'classnames';
import s from './ScrollWrapper.less';

const ScrollWrapper = ({ children, className }) => (
  <div className={cn(s.wrapper, className)}>{children}</div>
);

ScrollWrapper.propTypes = {
  children: propTypes.element.isRequired,
  className: propTypes.string,
};

export default ScrollWrapper;
