import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, FieldArray } from 'redux-form';
import { Button, Typography, Paper } from '@material-ui/core';
import TextInput from 'components/form/textInput';
import TextInputArray from 'components/form/textInputArray';
import Switch from 'components/form/switch';
import Breadcrumbs from 'components/breadcrumbs';
import RemoveConfirm from 'components/modal/removeConfirm';
import s from './EditSong.less';

class EditSong extends Component {
  constructor(props) {
    super(props);
    const { initialValues } = props;
    if (initialValues) {
      props.initialize(initialValues);
    }
  }

  state = {
    showQuestion: false,
  };

  deleteSong = () => {
    this.setState({ showQuestion: true });
  };

  hidePopup = () => {
    this.setState({ showQuestion: false });
  };

  render() {
    const { showQuestion } = this.state;
    const { handleSubmit, initialValues = {} } = this.props;
    const { name, id } = initialValues;
    const { deleteSong, hidePopup } = this;
    const { breadcrumbs } = EditSong;
    return (
      <Fragment>
        {showQuestion && (
          <RemoveConfirm
            url="/api/songs/delete"
            confirmString={name}
            id={id}
            close={hidePopup}
            redirect="/songs"
          />
        )}
        <Breadcrumbs crumbs={breadcrumbs} />
        <Paper className={s.paper}>
          <form onSubmit={handleSubmit} className={s.form}>
            <div className={s.caption}>
              <Field name="name" component={TextInput} label="Название" className={s.name} />
              <Field name="isCompleted" component={Switch} label="Готовность" />
            </div>
            <div className={s.notesContainer}>
              <Field
                name="notes"
                component={TextInput}
                label="Заметки"
                rowsMax={5}
                multiline
                className={s.notes}
              />
            </div>
            <Typography variant="title" gutterBottom>
              Аудиозаписи
            </Typography>
            <FieldArray
              name="audios"
              component={TextInputArray}
              fieldCaption="Запись"
              arrayName="audios"
              className={s.array}
            />
            <Typography variant="title" gutterBottom>
              Видеозаписи
            </Typography>
            <FieldArray
              name="videos"
              component={TextInputArray}
              fieldCaption="Видео"
              arrayName="videos"
              className={s.array}
            />
            <Button variant="contained" color="primary" type="submit">
              Сохранить
            </Button>
            {id && (
              <div className={s.remove}>
                <Button variant="outlined" onClick={deleteSong} size="small">
                  Удалить песню
                </Button>
              </div>
            )}
          </form>
        </Paper>
      </Fragment>
    );
  }
}

EditSong.validate = values => {
  const error = {};
  const { name, audios = [] } = values;
  if (!name) error.name = 'Название обязательно';
  if (audios && audios.length) {
    error.audios = audios.map(
      link =>
        link ? (link.match('soundcloud') ? null : 'Запись должна быть на soundcloud') : null,
    );
  }
  return error;
};

EditSong.breadcrumbs = [
  {
    label: 'Главная',
    to: '/',
  },
  {
    label: 'Список песен',
    to: '/songs',
  },
];

EditSong.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  initialize: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({
    name: PropTypes.string,
    isCompleted: PropTypes.bool,
    videos: PropTypes.arrayOf(PropTypes.string),
    audios: PropTypes.arrayOf(PropTypes.string),
    note: PropTypes.string,
    id: PropTypes.string,
  }),
};

export default reduxForm({
  form: 'Editable song',
  validate: EditSong.validate,
})(EditSong);
