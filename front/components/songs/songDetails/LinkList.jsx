import React from 'react';
import { List, ListItem } from '@material-ui/core';
import propTypes from 'prop-types';

const LinkList = ({ items }) => (
  <List>
    {items.map(item => (
      <ListItem key={item}>
        <a href={item} rel="noopener noreferrer" target="_blank">
          {item}
        </a>
      </ListItem>
    ))}
  </List>
);

LinkList.propTypes = {
  items: propTypes.arrayOf(propTypes.string),
};

export default LinkList;
