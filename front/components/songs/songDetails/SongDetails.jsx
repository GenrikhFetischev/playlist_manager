import React, { Component } from 'react';
import propTypes from 'prop-types';
import cn from 'classnames';
import {
  Tabs,
  Tab,
  Typography,
  Card,
  CardContent,
  List,
  ListItem,
  Button,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { CheckCircle } from '@material-ui/icons';
import LinkList from './LinkList';
import s from './SongDetails.less';

class SongDetails extends Component {
  state = {
    value: 'info',
    id: this.props.song.id,
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  static getDerivedStateFromProps(props, state) {
    const { song: { id } } = props;
    if (id === state.id) return state;
    return { id, value: 'info' };
  }

  render() {
    const { value } = this.state;
    const {
      song: { name, videos, audios, isCompleted, notes, playlists, id },
      className,
    } = this.props;
    const { handleChange } = this;
    return (
      <div className={cn(s.wrapper, className)}>
        <Card>
          <CardContent>
            <Tabs value={value} onChange={handleChange}>
              <Tab label="Инфо" value="info" />
              {audios && audios.length && <Tab label="Аудиозаписи" value="audios" />}
              {videos && videos.length && <Tab label="Видеозаписи" value="videos" />}
              {playlists && playlists.length && <Tab label="Где играли" value="playlists" />}
            </Tabs>
            {value === 'info' && (
              <Typography color="textSecondary" component="div">
                <h2 className={s.caption}>
                  {name} {isCompleted && <CheckCircle color="secondary" />}
                </h2>
                {notes && <div>{notes}</div>}
                <Button size="small">
                  <Link to={`/song/info/${id}`} className={s.edit}>
                    Редактировать
                  </Link>
                </Button>
              </Typography>
            )}
            {value === 'audios' && (
              <Typography color="textSecondary" component="div">
                <LinkList items={audios} />
              </Typography>
            )}
            {value === 'videos' && (
              <Typography color="textSecondary" component="div">
                <LinkList items={videos} />
              </Typography>
            )}
            {value === 'playlists' && (
              <List>
                {playlists.map(({ place, date }) => (
                  <ListItem key={date}>
                    <Typography color="textSecondary" component="div">
                      {`${place} ${new Date(date).toLocaleDateString()}`}
                    </Typography>
                  </ListItem>
                ))}
              </List>
            )}
          </CardContent>
        </Card>
      </div>
    );
  }
}

SongDetails.propTypes = {
  song: propTypes.shape({
    name: propTypes.string.isRequired,
    id: propTypes.string.isRequired,
    videos: propTypes.arrayOf(propTypes.string),
    audios: propTypes.arrayOf(propTypes.string),
    isCompleted: propTypes.bool,
    notes: propTypes.string,
  }),
  className: propTypes.string,
};

SongDetails.defaultProps = {
  song: {},
};

export default SongDetails;
