import React, { Component } from 'react';
import propTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import MusicIcon from '@material-ui/icons/MusicNote';
import Movie from '@material-ui/icons/Movie';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Done from '@material-ui/icons/Done';
import Typography from '@material-ui/core/Typography';
import s from './SongSnippet.less';

class SongSnippet extends Component {
  onClick = () => {
    const { item: { id }, onClick } = this.props;
    if (onClick) onClick(id);
  };

  render() {
    const { onClick } = this;
    const { name, videos, audios, isCompleted } = this.props.item;
    return (
      <Card onClick={onClick} className={s.wrapper}>
        <CardContent className={s.content}>
          <Avatar>
            <MusicIcon />
          </Avatar>
          <Typography variant="headline" component="h3" className={s.name}>
            {name}
          </Typography>
          {videos && videos.length && <Movie titleAccess="Видеозаписи" />}
          {audios && audios.length && <PlayArrow titleAccess="Аудиозаписи" />}
          {isCompleted && <Done titleAccess="Песня сделана" />}
        </CardContent>
      </Card>
    );
  }
}

SongSnippet.propTypes = {
  item: propTypes.shape({
    name: propTypes.string.isRequired,
    id: propTypes.string.isRequired,
    videos: propTypes.arrayOf(propTypes.string),
    audios: propTypes.arrayOf(propTypes.string),
    isCompleted: propTypes.bool,
  }),
  onClick: propTypes.func,
};

export default SongSnippet;
