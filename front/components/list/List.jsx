import React, { Component } from 'react';
import propTypes from 'prop-types';
import cn from 'classnames';
import ListUI from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import s from './List.less';

class List extends Component {
  render() {
    const { ListComponent, items, className, onItemClick } = this.props;
    return (
      <ListUI className={cn([s.wsrapper, className])}>
        {items.map((listItem, key) => (
          <ListItem key={listItem.id || key}>
            <ListComponent item={listItem} onClick={onItemClick} />
          </ListItem>
        ))}
      </ListUI>
    );
  }
}

List.propTypes = {
  ListComponent: propTypes.oneOfType([
    propTypes.shape({ render: propTypes.func.isRequired }),
    propTypes.func,
  ]),
  items: propTypes.array.isRequired,
  className: propTypes.string,
  onItemClick: propTypes.func,
};

export default List;
