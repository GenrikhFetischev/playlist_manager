import React from 'react';
import { Grid, Paper } from '@material-ui/core';
import { Link } from 'react-router-dom';
import propTypes from 'prop-types';

const Index = ({ to, text }) => (
  <Grid item lg={2} md={2}>
    <Paper elevation={12}>
      <Link to={to}>{text}</Link>
    </Paper>
  </Grid>
);

Index.propTypes = {
  to: propTypes.string.isRequired,
  text: propTypes.string,
};

export default Index;
