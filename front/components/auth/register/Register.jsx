import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, Form } from 'redux-form';
import { connect } from 'react-redux';
import cookie from 'browser-cookies';
import fetch from 'utils/fetch';
import { Button, Typography } from '@material-ui/core';
import TextInput from 'components/form/textInput';
import { login } from '../login/redux';
import s from './Register.less';

class Register extends Component {
  state = {
    fetching: false,
  };

  static getDerivedStateFromProps() {
    return {
      fetching: false,
    };
  }

  async register(data) {
    this.setState({ fetching: true });
    const user = await fetch('/auth/user/create', {
      method: 'POST',
      body: JSON.stringify(data),
    })
      .then(user => user.json())
      .catch(err => err);
    cookie.set('token', user.token, {expires: 365});
    this.props.login(user);
    this.setState({ fetching: false });
  }

  onSubmit = data => {
    this.register(data);
  };

  render() {
    const { onSubmit } = this;
    const { handleSubmit, auth } = this.props;
    const { fetching } = this.state;
    if (auth) return <Typography className={s.alreadyRegistred}>Вы зарегистрированы</Typography>;
    return (
      <Form onSubmit={handleSubmit(onSubmit)} className={s.form}>
        <Field component={TextInput} name="name" label="Имя" className={s.input} />
        <Field component={TextInput} name="surname" label="Фамилия" className={s.input} />
        <Field component={TextInput} name="instrument" label="Инструмент" className={s.input} />
        <Field component={TextInput} name="login" label="Логин" className={s.input} />
        <Field
          component={TextInput}
          name="password"
          label="Пароль"
          className={s.input}
          type="password"
        />
        <Button type="submit" disabled={fetching} className={s.button} variant="outlined">
          Зарегистрироваться
        </Button>
      </Form>
    );
  }
}

Register.propTypes = {
  handleSubmit: PropTypes.func,
  login: PropTypes.func,
  className: PropTypes.string,
  auth: PropTypes.object,
};

const mapStateToProps = ({ auth = null }) => ({
  auth,
});
const mapDispatchToProps = dispatch => ({
  login: data => dispatch(login(data)),
});

export default reduxForm({
  form: 'register',
})(connect(mapStateToProps, mapDispatchToProps)(Register));
