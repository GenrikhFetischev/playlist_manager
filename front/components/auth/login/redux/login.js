import { createActions, handleAction, combineActions } from 'redux-actions';
import cookie from 'browser-cookies';
import fetch from 'utils/fetch';

const { login, logout } = createActions({
  LOGIN: payload => payload,
  LOGOUT: payload => payload,
});

const auth = body => async dispatch => {
  const options = {
    method: 'POST',
    body: JSON.stringify(body),
  };
  const loginAttempt = await fetch('/auth/user/login', options).then(response => response.json());
  if (loginAttempt.token) {
    cookie.set('token', loginAttempt.token, { expires: 365 });
    dispatch(login(loginAttempt));
  } else {
    cookie.erase('token');
    dispatch(logout(null));
  }
};

const reducer = handleAction(
  combineActions(login, logout),
  {
    next: (state, { payload }) => payload,
    throw: state => state,
  },
  null,
);

export default reducer;
export { login, logout, auth };
