import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, Form } from 'redux-form';
import { Button } from '@material-ui/core';
import TextInput from 'components/form/textInput';
import { connect } from 'react-redux';
import { auth } from './redux';
import s from './Login.less';

class Login extends Component {
  state = {
    fetching: false,
  };

  static getDerivedStateFromProps() {
    return {
      fetching: false,
    };
  }

  onSubmit = data => {
    this.setState({ fetching: true });
    this.props.login(data);
  };

  render() {
    const { onSubmit } = this;
    const { handleSubmit } = this.props;
    const { fetching } = this.state;
    return (
      <Form onSubmit={handleSubmit(onSubmit)} className={s.form}>
        <Field component={TextInput} name="login" label="Логин" className={s.input} />
        <Field component={TextInput} name="password" label="Пароль" className={s.input} type="password"/>
        <Button type="submit" disabled={fetching} className={s.button} variant="outlined">
          Войти
        </Button>
      </Form>
    );
  }
}

Login.propTypes = {
  handleSubmit: PropTypes.func,
  login: PropTypes.func,
  className: PropTypes.string,
};

const mapStateToProps = ({ auth = null }) => ({
  auth,
});
const mapDispatchToProps = dispatch => ({
  login: data => dispatch(auth(data)),
});

export default reduxForm({
  form: 'login',
})(connect(mapStateToProps, mapDispatchToProps)(Login));
