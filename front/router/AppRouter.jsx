import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import MainPage from 'pages/main';
import Songs from 'pages/songs';
import Playlists from 'pages/playlists';
import AddSong from 'pages/addSong';
import AddPlaylist from 'pages/addPlaylist';
import Playlist from 'pages/playlist';
import Song from 'pages/song';
import MainLayout from 'components/mainLayout';
import cookie from 'browser-cookies';

import { connect } from 'react-redux';

const AppRouter = () => (
  <div className="application">
    <Router>
      <MainLayout>
        <Switch>
          <Route path="/">
            <div>
              {!cookie.get('token') && <Redirect to="/" />}
              <Route exact path="/" component={MainPage} />
              <Route exact path="/songs" component={Songs} />
              <Route exact path="/song/create" component={AddSong} />
              <Route path="/song/info/:id" component={Song} />
              <Route exact path="/playlists" component={Playlists} />
              <Route exact path="/playlist/create" component={AddPlaylist} />
              <Route path="/playlists/:id" component={Playlist} />
            </div>
          </Route>
        </Switch>
      </MainLayout>
    </Router>
  </div>
);

AppRouter.propTypes = {
  auth: PropTypes.object,
};

const mapStateToProps = ({ auth }) => ({ auth });

export default connect(mapStateToProps)(AppRouter);
