import React, { Component } from 'react';
import propTypes from 'prop-types';
import fetch from 'utils/fetch';
import List from 'components/list';
import SongSnippet from 'components/songs/songSnippet';
import SongDetails from 'components/songs/songDetails';
import GetterInitialProps from 'hocs/GetterInitialProps';
import ScrollWrapper from 'components/scrollWrapper';
import s from './songs.less';

class Songs extends Component {
  state = {};

  static async getInitialProps() {
    return await fetch('/api/songs').then(resp => resp.json());
  }

  async selectSong(id) {
    const { initialProps: { items: songs } } = this.props;
    const selectedSong = songs.find(song => song.id === id);
    const { playlists } = await fetch(`/api/songs/${selectedSong.id}`).then(response =>
      response.json(),
    );
    this.setState({
      selectedSong: {
        ...selectedSong,
        playlists,
      },
    });
  }

  render() {
    const { selectSong } = this;
    const { initialProps: { items: songs } } = this.props;
    const { selectedSong } = this.state;
    return (
      <div className={s.wrapper}>
        <ScrollWrapper>
          <List items={songs} ListComponent={SongSnippet} onItemClick={selectSong.bind(this)} />
        </ScrollWrapper>
        {selectedSong && (
          <ScrollWrapper>
            <SongDetails song={selectedSong} />
          </ScrollWrapper>
        )}
      </div>
    );
  }
}

Songs.propTypes = {
  initialProps: propTypes.shape({
    items: propTypes.array,
  }),
};

export default GetterInitialProps(Songs);
