import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GetterInitialProps from 'hocs/GetterInitialProps';
import EditPlaylist from 'components/playlists/editPlaylist';
import fetch from 'utils/fetch';

class PlaylistPage extends Component {
  static async getInitialProps(props) {
    const { id } = props.match.params;
    const [playlist, songs] = await Promise.all([
      fetch(`/api/playlists/${id}`).then(playlist => playlist.json()),
      fetch(`/api/songs/`).then(playlist => playlist.json()),
    ]);
    return {
      playlist: playlist.items[0],
      songList: songs.items,
    };
  }

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {};
  }

  async onSubmit(data) {
    this.setState({ fetching: true });
    const response = await fetch('/api/playlists', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json',
      },
    }).then(resp => resp.json());
    this.setState({ response, fetching: false });
  }

  render() {
    const { onSubmit } = this;
    const { fetching } = this.state;
    const { playlist, songList } = this.props.initialProps;
    return (
      <div>
        <EditPlaylist
          onSubmit={onSubmit}
          initialValues={playlist}
          songList={songList}
          fetching={fetching}
        />
      </div>
    );
  }
}

PlaylistPage.propTypes = {
  initialProps: PropTypes.shape({
    playlist: PropTypes.object,
    songList: PropTypes.array,
  }),
};

export default GetterInitialProps(PlaylistPage);
