import React, { Component } from 'react';
import EditSong from 'components/songs/editSong';
import GetterInitialProps from 'hocs/GetterInitialProps';
import fetch from 'utils/fetch';

class AddSongPage extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {};
  }

  static async getInitialProps() {
    const { items } = await fetch('/api/songs')
      .then(songlist => songlist.json())
      .catch(console.warn);
    return { items };
  }

  async onSubmit(data) {
    const body = JSON.stringify(data);
    const resp = await fetch('/api/songs', {
      method: 'POST',
      body,
    }).then(r => r.json());
    this.setState({ resp });
  }

  render() {
    const { onSubmit } = this;
    return (
      <div>
        <EditSong onSubmit={onSubmit} />
      </div>
    );
  }
}

export default GetterInitialProps(AddSongPage);
