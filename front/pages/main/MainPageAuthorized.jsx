import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'utils/fetch';
import { Typography } from '@material-ui/core';
import PlaylistSnippet from 'components/playlists/playlistSnippet';
import PlaylistDetails from 'components/playlists/playlistDetails';
import SongSnippet from 'components/songs/songSnippet';
import SongDetails from 'components/songs/songDetails';
import List from 'components/list';
import ScrollWrappper from 'components/scrollWrapper';
import s from './MainPage.less';

class MainPageUnauthorized extends Component {
  state = {
    futurePlaylists: [],
    unReadySongs: [],
  };

  async componentDidMount() {
    const [futurePlaylists, unReadySongs] = await Promise.all([
      fetch('/api/playlists/future/list').then(list => list.json()),
      fetch('/api/songs')
        .then(list => list.json())
        .then(({ items = [] }) => items.filter(song => !song.isCompleted)),
    ]);
    this.setState({ futurePlaylists, unReadySongs });
  }

  songSelect = songId => {
    const { unReadySongs } = this.state;
    this.setState({ selectedSong: unReadySongs.find(({ id }) => id === songId) });
  };

  playlistSelect = playlistId => {
    const { futurePlaylists } = this.state;
    this.setState({ selectedPlaylist: futurePlaylists.find(({ id }) => id === playlistId) });
  };

  render() {
    const { songSelect, playlistSelect } = this;
    const { futurePlaylists, unReadySongs, selectedSong, selectedPlaylist } = this.state;
    return (
      <div className={s.wrapper}>
        <Typography variant="headline">Плейлисты для ближайших концертов</Typography>
        <section className={s.playlists}>
          {futurePlaylists[0] && (
            <ScrollWrappper className={s.scrollWrapper}>
              <List
                items={futurePlaylists}
                ListComponent={PlaylistSnippet}
                onItemClick={playlistSelect}
              />
            </ScrollWrappper>
          )}
          <div className={s.selected}>
            {selectedPlaylist && <PlaylistDetails playlist={selectedPlaylist} />}
          </div>
        </section>

        <Typography variant="headline">Не готовые песни</Typography>
        <section className={s.songs}>
          {unReadySongs[0] && (
            <List items={unReadySongs} ListComponent={SongSnippet} onItemClick={songSelect} />
          )}
          <div className={s.selected}>{selectedSong && <SongDetails song={selectedSong} />}</div>
        </section>
      </div>
    );
  }
}

MainPageUnauthorized.propTypes = {
  auth: PropTypes.object,
};

export default MainPageUnauthorized;
