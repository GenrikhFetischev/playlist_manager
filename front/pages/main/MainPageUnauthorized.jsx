import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Login from 'components/auth/login';
import Register from 'components/auth/register';
import { Card, CardContent, Tabs, Tab } from '@material-ui/core';
import s from './MainPage.less';

class MainPageUnauthorized extends Component {
  state = {
    formType: 'login',
  };

  switchForm = (event, value) => {
    this.setState({
      formType: value,
    });
  };

  render() {
    const { switchForm } = this;
    const { formType } = this.state;
    return (
      <div className={s.wrapperUnauthorized}>
        <Card>
          <CardContent>
            <Tabs onChange={switchForm} value={formType}>
              <Tab label="Войти" value="login" />
              <Tab label="Зарегистрироваться" value="register" />
            </Tabs>
            {formType === 'register' && <Register />}
            {formType === 'login' && <Login />}
          </CardContent>
        </Card>
      </div>
    );
  }
}

MainPageUnauthorized.propTypes = {
  auth: PropTypes.object,
};

export default MainPageUnauthorized;
