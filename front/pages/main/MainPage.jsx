import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Authorized from './MainPageAuthorized';
import Unauthorized from './MainPageUnauthorized';

const MainPage = ({ auth }) => (auth ? <Authorized /> : <Unauthorized />);

MainPage.propTypes = {
  auth: PropTypes.object,
};

const mapStateToProps = ({ auth }) => ({ auth });

export default connect(mapStateToProps)(MainPage);
