import React, { Component } from 'react';
import propTypes from 'prop-types';
import GetterInitialProps from 'hocs/GetterInitialProps';
import PlaylistSnippet from 'components/playlists/playlistSnippet';
import PlaylistDetails from 'components/playlists/playlistDetails';
import List from 'components/list';
import ScrollWrapper from 'components/scrollWrapper';
import fetch from 'utils/fetch';

import s from './Playlists.less';

class Playlists extends Component {
  static async getInitialProps() {
    return await fetch('/api/playlists').then(r => r.json());
  }

  state = {};

  selectPlaylist = id => {
    const { items } = this.props.initialProps;
    const selectedPlaylist = items.find(item => item.id === id);
    this.setState({
      selectedPlaylist,
    });
  };

  render() {
    const { selectPlaylist } = this;
    const { items } = this.props.initialProps;
    const { selectedPlaylist } = this.state;
    return (
      <div className={s.wrapper}>
        <ScrollWrapper>
          <List items={items} ListComponent={PlaylistSnippet} onItemClick={selectPlaylist} />
        </ScrollWrapper>

        {selectedPlaylist && (
          <div className={s.animated} key={selectedPlaylist.id}>
            <ScrollWrapper>
              <PlaylistDetails playlist={selectedPlaylist} />
            </ScrollWrapper>
          </div>
        )}
      </div>
    );
  }
}

Playlists.propTypes = {
  initialProps: propTypes.shape({
    items: propTypes.array,
  }),
};

export default GetterInitialProps(Playlists);
