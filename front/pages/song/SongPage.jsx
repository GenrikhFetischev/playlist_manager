import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GetterInitialProps from 'hocs/GetterInitialProps';
import EditSong from 'components/songs/editSong';
import fetch from 'utils/fetch';

class SongPage extends Component {
  static async getInitialProps(props) {
    const { id } = props.match.params;
    const song = await fetch(`/api/songs/${id}`)
      .then(song => song.json())
      .catch(err => console.warn(err));
    return { song };
  }

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {};
  }

  async onSubmit(data) {
    const body = JSON.stringify(data);
    const updated = await fetch('/api/songs', {
      method: 'POST',
      body: body,
    })
      .then(r => r.json())
      .catch(e => {
        console.warn(e);
      });
    this.setState({ updatedSong: updated.response });
  }

  render() {
    const { song } = this.props.initialProps;
    const { updatedSong } = this.state;
    const { onSubmit } = this;

    return (
      <div>
        <EditSong initialValues={updatedSong || song} onSubmit={onSubmit} />
      </div>
    );
  }
}

SongPage.propTypes = {
  initialProps: PropTypes.shape({
    song: PropTypes.object,
  }),
};

export default GetterInitialProps(SongPage);
