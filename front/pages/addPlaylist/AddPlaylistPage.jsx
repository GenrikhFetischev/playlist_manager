import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AddPlaylist from 'components/playlists/editPlaylist';
import GetterInitialProps from 'hocs/GetterInitialProps';
import fetch from 'utils/fetch';

class AddPlaylistPage extends Component {
  static async getInitialProps() {
    const { items: songList } = await fetch('/api/songs').then(resp => resp.json());
    return { songList };
  }

  constructor(props) {
    super(props);
    this.state = {};
    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(data) {
    this.setState({ fetching: true });
    const response = await fetch('/api/playlists', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json',
      },
    }).then(resp => resp.json());
    this.setState({ response, fetching: false });
  }

  render() {
    const { onSubmit } = this;
    const { initialProps: { songList } } = this.props;
    const { fetching } = this.state;
    return (
      <div>
        <AddPlaylist songList={songList} onSubmit={onSubmit} fetching={fetching} />
      </div>
    );
  }
}

AddPlaylistPage.propTypes = {
  initialProps: PropTypes.shape({
    songList: PropTypes.array,
  }),
};

export default GetterInitialProps(AddPlaylistPage);
