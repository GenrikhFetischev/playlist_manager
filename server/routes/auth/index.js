const Router = require('koa-router');
const compose = require('koa-compose');
const mount = require('koa-mount');

const user = require('./user');

const router = new Router();

router.use('/user', user.routes(), user.allowedMethods());

module.exports = mount('/auth', compose([router.routes(), router.allowedMethods()]));
