const jwt = require('jsonwebtoken');
const { secret } = require('../../../config/index');

module.exports = (id, expiresIn = '10y') => {
  return jwt.sign({ id }, secret.secret, {
    expiresIn,
  });
};
