const Router = require('koa-router');
const bcrypt = require('bcryptjs');
const getItemsFromIdlist = require('../api/utils/getItemsFromIdList');
const createToken = require('./utils/createToken');
const verifyToken = require('../accessControl/utils/verifyToken');

const router = new Router();

router.post('/me', async (ctx, next) => {
  const { authorization } = ctx.request.headers;
  const { id } = verifyToken(authorization);
  if (!id) {
    ctx.status = 422;
    ctx.body = {
      error: 'User not finded',
    };
    return next();
  }
  ctx.body = {
    items: await getItemsFromIdlist(ctx.models.User, [id], ctx.models.Group),
  };
});

router.post('/create', async ctx => {
  const { login, password, name, groupId, surname, instrument } = ctx.request.body;
  const { Group, User } = ctx.models;
  const hashedPassword = bcrypt.hashSync(password, 8);
  const group = await Group.find({ where: { id: groupId } });
  const user = await User.create({ login, name, surname, instrument, password: hashedPassword });
  const token = createToken(user.id);
  ctx.set('authorization', token);
  ctx.body = {
    group: group ? await group.addUser(user) : undefined,
    user,
    token,
  };
});

router.post('/login', async ctx => {
  const { login, password } = ctx.request.body;
  if (!login || !password) {
    ctx.body = {
      error: 'Login and password is required',
    };
    return;
  }
  const { Group, User } = ctx.models;
  const user = await User.find({ where: { login: login }, include: Group });
  const passwordIsValid = bcrypt.compareSync(password, user.dataValues.password);
  if (passwordIsValid) {
    const token = await createToken(user.id);
    ctx.set('authorization', token);
    ctx.body = {
      user,
      token,
    };
  } else {
    ctx.body = {
      errors: {
        password: 'Неправильный пароль',
      },
    };
  }
});

module.exports = router;
