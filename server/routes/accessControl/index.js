const verifyToken = require('./utils/verifyToken');

const privateUrls = ['/api', '/auth/user/me'];

const isPrivate = url =>
  privateUrls.reduce((isPrivate, item) => {
    if (url.startsWith(item)) return true;
    return isPrivate;
  }, false);

const accessControl = async (ctx, next) => {
  if (!isPrivate(ctx.path)) return next();
  const { authorization } = ctx.request.headers;
  if (verifyToken(authorization).error) {
    ctx.status = 401;
    ctx.body = { error: 'Authorization required' };
  } else {
    return next();
  }
};

module.exports = accessControl;
