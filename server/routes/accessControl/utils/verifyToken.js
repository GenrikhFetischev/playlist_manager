const jwt = require('jsonwebtoken');
const { secret } = require('../../../config/index');

module.exports = token => {
  try {
    const res = jwt.verify(token, secret.secret);
    return res;
  } catch (e) {
    return {
      ...e,
      error: true,
    };
  }
};
