const Router = require('koa-router');
const updateOrCreate = require('./utils/updateOrCreate');
const getItemsFromIdlist = require('./utils/getItemsFromIdList');
const removeItem = require('./utils/removeItem');

const router = new Router();

router.get('/', async (ctx, next) => {
  ctx.body = { items: await ctx.models.Playlist.findAll({ include: ctx.models.Song }) };
  return next();
});
router.get('/:id', async (ctx, next) => {
  ctx.body = {
    items: await getItemsFromIdlist(ctx.models.Playlist, [ctx.params.id], ctx.models.Song),
  };
  return next();
});

router.post('/', async (ctx, next) => {
  const created = await updateOrCreate(ctx, ctx.models.Playlist);
  const { songList } = ctx.request.body;
  if (!songList) return (ctx.body = created);
  const songs = await getItemsFromIdlist(ctx.models.Song, songList);
  ctx.body = await created.response
    .setSongs(songs)
    .then(response => ({ response, status: 'OK' }))
    .catch(error => ({ error }));
  return next();
});

router.get('/future/list', async (ctx, next) => {
  const { date = new Date().toUTCString(), count = 10 } = ctx.query;
  const { models: { Playlist, Song }, Op } = ctx;
  ctx.body = await Playlist.all({
    where: { date: { [Op.gt]: date } },
    limit: count,
    include: Song,
  });
  return next();
});

router.post('/delete', async (ctx, next) => {
  ctx.body = await removeItem(ctx, ctx.models.Playlist);
  return next();
});

module.exports = router;
