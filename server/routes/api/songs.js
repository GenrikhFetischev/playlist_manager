const Router = require('koa-router');
const updateOrCreate = require('./utils/updateOrCreate');
const getItemsFromIdlist = require('./utils/getItemsFromIdList');
const removeItem = require('./utils/removeItem');

const router = new Router();

router.get('/', async (ctx, next) => {
  ctx.body = { items: await ctx.models.Song.findAll() };
  return next();
});
router.get('/:id', async (ctx, next) => {
  const { models: { Song, Playlist }, params: { id } } = ctx;
  const [item] = await getItemsFromIdlist(Song, [id]);
  const playlists = await Playlist.findAndCountAll({
    include: [{ model: Song, where: { id } }],
    attributes: ['date', 'place'],
  });
  ctx.body = {
    ...item.dataValues,
    playlists: playlists.rows,
  };
  return next();
});
router.post('/', async (ctx, next) => {
  ctx.body = await updateOrCreate(ctx, ctx.models.Song);
  return next();
});

router.post('/delete', async (ctx, next) => {
  ctx.body = await removeItem(ctx, ctx.models.Song);
  return next();
});

module.exports = router;
