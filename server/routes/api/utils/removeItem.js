module.exports = async (ctx, model, force) => {
  const { id } = ctx.request.body;
  const destroy = await model.destroy({ where: { id }, force });
  return destroy ? { status: 'DELETED' } : { error: 'NOT FOUNDED ITEM' };
};
