module.exports = async (Model, idList, include) =>
  await Promise.all(idList.map(id => Model.find({ where: {id}, include })));
