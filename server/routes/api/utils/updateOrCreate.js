module.exports = async (ctx, Model) => {
  const { body, body: { id } } = ctx.request;
  if (id) {
    const instance = await Model.find({ where: { id } });
    return await Model.update(body, { where: { id } })
      .then(() => ({ status: 'UPDATED', response: instance, id: instance.dataValues.id }))
      .catch(error => ({ error }));
  } else {
    return await Model.create(body)
      .then(response => ({ response, status: 'OK', id: response.dataValues.id }))
      .catch(error => ({ error }));
  }
};
