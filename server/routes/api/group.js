const Router = require('koa-router');
const router = new Router();
const getItemsFromIdlist = require('./utils/getItemsFromIdList');

router.get('/:id', async ctx => {
  const { Group, User, Playlist, Song } = ctx.models;
  ctx.body = {
    items: await getItemsFromIdlist(Group, [ctx.params.id], [User, Playlist, Song]),
  };
});

router.post('/create', async ctx => {
  const { name } = ctx.request.body;
  ctx.body = await ctx.models.Group.create({ name });
});

module.exports = router;
