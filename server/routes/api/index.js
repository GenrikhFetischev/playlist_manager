const Router = require('koa-router');
const compose = require('koa-compose');
const mount = require('koa-mount');

const songs = require('./songs');
const playlists = require('./playlists');
const group = require('./group');

const router = new Router();
router.use('/songs', songs.routes(), songs.allowedMethods());
router.use('/playlists', playlists.routes(), playlists.allowedMethods());
router.use('/group', group.routes(), group.allowedMethods());

module.exports = mount('/api', compose([router.routes(), router.allowedMethods()]));
