const path = require('path');
const koaWebpack = require('koa-webpack');
const config = require('../webpack.config');
const { path: outputPath } = config.output;

module.exports = async app => {
  const webpackMiddleware = await koaWebpack({ config });
  app.use(webpackMiddleware);
  app.use(async (ctx, next) => {
    if (ctx.path.startsWith('/api')) return next();
    if (ctx.path.startsWith('/auth')) return next();
    const fs = webpackMiddleware.devMiddleware.fileSystem;
    ctx.type = 'html';
    ctx.body = fs.createReadStream(path.join(outputPath, 'index.html'));
  });
};
