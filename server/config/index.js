const secret = require('./secret')

module.exports = {
  PORT: 3033,
  DB_URL: 'postgres://localhost:5432/songs_new',
  secret
};
