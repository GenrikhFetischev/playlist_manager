const tables = Sq => [
  [
    'Songs',
    {
      id: {
        type: Sq.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sq.STRING,
        allowNull: false,
        unique: true,
      },
      isCompleted: {
        type: Sq.BOOLEAN,
        defaultValue: false,
      },
      videos: {
        type: Sq.JSON,
      },
      audios: {
        type: Sq.JSON,
      },
      notes: {
        type: Sq.string,
      },
    },
  ],
  [
    'Playlists',
    {
      id: {
        type: Sq.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      place: {
        type: Sq.STRING,
        allowNull: false,
      },
      date: {
        type: Sq.DATE,
        allowNull: false,
      },
    },
  ],
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await tables(Sequelize).reduce(
      (chain, [table, schema]) => chain.then(() => queryInterface.createTable(table, schema)),
      Promise.resolve(),
    );
  },
  down: async (queryInterface, Sequelize) => {
    await tables(Sequelize)
      .reverse()
      .reduce(
        (chain, [table]) => chain.then(() => queryInterface.dropTable(table)),
        Promise.resolve(),
      );
  },
};
