const Sq = require('sequelize');

class Song extends Sq.Model {
  static associate({ Playlist }) {
    Song.belongsToMany(Playlist, { through: 'PlaylistSongs' });
  }
}

const songSchema = {
  id: {
    type: Sq.UUID,
    defaultValue: Sq.UUIDV4,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: Sq.STRING,
    allowNull: false,
    unique: true,
  },
  isCompleted: {
    type: Sq.BOOLEAN,
    defaultValue: false,
  },
  videos: {
    type: Sq.JSON,
  },
  audios: {
    type: Sq.JSON,
  },
  notes: {
    type: Sq.STRING,
  },
};
module.exports = sequelize => Song.init(songSchema, { sequelize });
