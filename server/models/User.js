const Sq = require('sequelize');

class User extends Sq.Model {
  static associate({ Group }) {
    User.belongsTo(Group);
  }
}

const userSchema = {
  id: {
    type: Sq.UUID,
    defaultValue: Sq.UUIDV4,
    allowNull: false,
    primaryKey: true,
  },
  login: {
    type: Sq.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: Sq.STRING,
    allowNull: false,
  },
  surname: {
    type: Sq.STRING,
  },
  name: {
    type: Sq.STRING,
    allowNull: false,
  },
  instrument: {
    type: Sq.STRING,
  },
};
module.exports = sequelize => User.init(userSchema, { sequelize });
