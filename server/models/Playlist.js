const Sq = require('sequelize');

class Playlist extends Sq.Model {
  static associate({ Song }) {
    Playlist.belongsToMany(Song, { through: 'SongPlaylist' });
  }
}

const playlistSchema = {
  id: {
    type: Sq.UUID,
    defaultValue: Sq.UUIDV4,
    allowNull: false,
    primaryKey: true,
  },
  place: {
    type: Sq.STRING,
    allowNull: false,
  },
  date: {
    type: Sq.DATE,
    allowNull: false,
    unique: true,
  },
};

module.exports = sequelize => Playlist.init(playlistSchema, { sequelize });
