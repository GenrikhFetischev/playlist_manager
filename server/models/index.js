const { readdirSync } = require('fs');
const { join, basename } = require('path');

module.exports = sequelize => {
  const currentFile = basename(__filename);
  readdirSync(__dirname)
    .filter(file => file !== currentFile)
    .map(modelName => require(join(__dirname, modelName)))
    .map(loadModel => loadModel(sequelize))
    .forEach(model => {
      if (typeof model.associate === 'function') {
        model.associate(sequelize.models);
      }
    });
};
