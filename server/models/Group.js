const Sq = require('sequelize');

class Group extends Sq.Model {
  static associate({ Playlist, Song, User }) {
    Group.hasMany(Playlist);
    Group.hasMany(Song);
    Group.hasMany(User);
  }
}

const GroupSchema = {
  id: {
    type: Sq.UUID,
    defaultValue: Sq.UUIDV4,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: Sq.STRING,
    allowNull: false,
  },
};
module.exports = sequelize => Group.init(GroupSchema, { sequelize });
