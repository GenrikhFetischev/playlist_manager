const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const Sequelize = require('sequelize');

const api = require('./routes/api');
const auth = require('./routes/auth');
const accessControl = require('./routes/accessControl');
const { DB_URL, PORT } = require('./config');
const devMiddleware = require('./frontDev');

const isDev = process.env.NODE_ENV !== 'production';

const sq = new Sequelize(DB_URL);

require('./models')(sq);
const app = new Koa();

app.context.sequelize = sq;
app.context.models = sq.models;
app.context.Op = Sequelize.Op;

app.use(bodyParser());
app.use(accessControl);

app.use(auth);
app.use(api);

if (isDev) {
  devMiddleware(app);
}

app.listen(PORT);
